#
# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_COPY_FILES += \
	device/renesas/mackerel/init.mackerel.rc:root/init.mackerel.rc \
	device/renesas/mackerel/media_profiles.xml:system/etc/media_profiles.xml \
	device/renesas/mackerel/mackerel-tca6416-keypad.kl:system/usr/keylayout/mackerel-tca6416-keypad.kl \
	device/renesas/mackerel/mackerel-tca6416-keypad.kcm:system/usr/keychars/mackerel-tca6416-keypad.kcm \
	device/renesas/mackerel/vold.fstab:system/etc/vold.fstab
